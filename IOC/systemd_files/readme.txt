"ioc_tms.service" starts a procServ service named "ioc_tms", which automatically starts the TMS IOC.
------------------------------------------------

1.) First, check the contents of "ioc_tms.service". Make sure it points to the correct location of the epics directory and the e3 tools source environment, according to the file structure on this computer!

2.) Copy file "ioc_tms.service" to directory:
    /etc/systemd/system


Terminal commands:
------------------------------------------------
- Status:
   $ systemctl status ioc_tms

- Start/re-start service:
   $ sudo systemctl start ioc_tms

- Stop service:
   $ sudo systemctl stop ioc_tms

- Enable service:
   $ sudo systemctl enable ioc_tms

- Disable:
   $ sudo systemctl disable ioc_tms


Telnet commands:
------------------------------------------------
Access the IOC shell using Telnet:
   $ telnet localhost 2000

Telnet terminal commands: 
- Exit:
   $ Ctrl + ] , followed by "quit"
- Restart:
   $ Ctrl + X
