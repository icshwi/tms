cd ~/ics_plc_factory
python plcfactory.py -d Tgt-TMS1063:Ctrl-PLC-01 --plc-beckhoff --e3=Tgt-TMS1063_Ctrl-PLC-01
cp ~/ics_plc_factory/output/tgt-tms1063_ctrl-plc-01/PLCFactory_external_source_Beckhoff.zip ~/tms/PLC/plcfactory/ 
cp ~/tms/IOC/e3-tgt-tms1063_ctrl-plc-01/st.cmd ~/ics_plc_factory/output/tgt-tms1063_ctrl-plc-01/modules/e3-tgt-tms1063_ctrl-plc-01/
cp ~/tms/IOC/e3-tgt-tms1063_ctrl-plc-01/configure/CONFIG_MODULE  ~/ics_plc_factory/output/tgt-tms1063_ctrl-plc-01/modules/e3-tgt-tms1063_ctrl-plc-01/configure/
source ~/e3/tools/setenv
cd ~/ics_plc_factory/output/tgt-tms1063_ctrl-plc-01/modules/e3-tgt-tms1063_ctrl-plc-01/
make build
make install

