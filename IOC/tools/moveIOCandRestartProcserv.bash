# Move IOC from plcfactory to tms repo
cp -rf ~/ics_plc_factory/output/tgt-tms1063_ctrl-plc-01/modules/e3-tgt-tms1063_ctrl-plc-01/ ~/tms/IOC/ 

# Restart IOC service (assumming it is already set up in etc/systemd/system)
#sudo systemctl stop ioc_tms
sudo systemctl start ioc_tms
