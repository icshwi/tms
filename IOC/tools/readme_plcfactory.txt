Instructions for running plcfactory to re-generate the IOC
-----------------------------------------------------------
-----------------------------------------------------------
How to run script "makenew_TMS_IOC.bash":
-----------------------------------------------------------
$ source makenew_TMS_IOC.bash

NOTE: Calling the script using 'source' causes the shell to end in the new IOC directory, which is handy. Otherwise, if script is called using 'bash', you end up in the same current directory as where you started.


What does it do?
-----------------------------------------------------------
1.) Goes into the plcfactory directory
2.) Runs the python script, which builds the IOC 
	- for PLC: i.e. "Tgt-TMS1063:Ctrl-PLC-01" (according to the slot in CCDB)
	- IOC name: i.e. "Tgt-TMS1063:Ctrl-PLC-01"
3.) Copies PLCFactory_external_source_Beckhoff.zip to the repo.
4.) Copies "st.cmd" and "CONFIG_MODULE" from repo to the IOC directory in the plcfactory tree. 
	NOTES: 
	a) Files are copied to plcfactory directory in order to test-run the IOC directly from that location.
	B) Make sure the config files are actually configured for the epics modules and ip addresses that are actually installed.
5.) Sets the e3 environment. ($ source ~/e3/tools/setenv)
6.) Goes into the IOC directory in the plcfactory tree.
7.) Builds and Installs the new IOC.


What to do next - Transfer the PLC code to Twincat project
-----------------------------------------------------------
1.) Got ~/tms/PLC/plcfactory
2.) git add, git commit, git push
3.) Move over to Twincat engineering PC --> git pull
4.) Stop PLC. Delete EPICS folder, import new zip file
5.) Build and run new PLC appliction.
6.) Move back over to IOC / Linux machine...


What to do next - Test the IOC..
-----------------------------------------------------------
1.) Start the IOC:
	$ iocsh st.cmd

2.) Check messages, check contents of PV_LIST.txt
3.) Check the PVs using camonitor:
	1.) Open another command window
	2.) Set the e3 environment: $ source ~/e3/tools/setenv
	3.) Get a PV name to test: e.g. 
		1.) Goto the IOC directory
		2.) $ cat PV_LIST.txt | grep Ch1
		3.) Copy the name
	4.) $ camonitor Tgt-TMS1063:Proc-GU-102:FB_Ch1_On


Copy new IOC to repo directory
-----------------------------------------------------------
1.) $ mv -rf ~/
