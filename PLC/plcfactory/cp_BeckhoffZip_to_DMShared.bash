# Copy Beckhoff source code zip file from plcfactory output to shared folder on development machine and to repo
cp ~/ics_plc_factory/output/tgt-tms1063_ctrl-plc-01/PLCFactory_external_source_Beckhoff.zip /data
cp ~/ics_plc_factory/output/tgt-tms1063_ctrl-plc-01/PLCFactory_external_source_Beckhoff.zip ~/tms/PLC/plcfactory
