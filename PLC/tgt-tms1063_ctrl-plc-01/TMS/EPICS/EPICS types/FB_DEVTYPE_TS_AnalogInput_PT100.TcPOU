﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4024.3">
  <POU Name="FB_DEVTYPE_TS_AnalogInput_PT100" Id="{ce17ea3e-6a40-4ca7-ad3b-e9357cd15771}" SpecialFunc="None">
    <Declaration><![CDATA[
FUNCTION_BLOCK FB_DEVTYPE_TS_AnalogInput_PT100
VAR_INPUT
      nOffsetStatus	:INT;			//Offset for status variables
	  nOffsetCmd   :INT;			//Offset for command variables
      nOffsetPar   :INT;			//Offset for parameter variables
      HWIO_TerminalName  :STRING;        //EPICS Status variable: HWIO_TerminalName
      HWIO_ChannelNo  :STRING;        //EPICS Status variable: HWIO_ChannelNo
      HWIO_Fault_Act  :BOOL;        //EPICS Status variable: HWIO_Fault_Act
      HWIO_Fault_Lchd  :BOOL;        //EPICS Status variable: HWIO_Fault_Lchd
      Force_En_RB  :BOOL;        //EPICS Status variable: Force_En_RB
      OpMode_FreeRun_Act  :BOOL;        //EPICS Status variable: OpMode_FreeRun_Act
      OpMode_Forced_Act  :BOOL;        //EPICS Status variable: OpMode_Forced_Act
      ForceValue_RB  :REAL;        //EPICS Status variable: ForceValue_RB
      MeasValue  :REAL;        //EPICS Status variable: MeasValue
      H2_Monitor_En_RB  :BOOL;        //EPICS Status variable: H2_Monitor_En_RB
      H1_Monitor_En_RB  :BOOL;        //EPICS Status variable: H1_Monitor_En_RB
      L1_Monitor_En_RB  :BOOL;        //EPICS Status variable: L1_Monitor_En_RB
      L2_Monitor_En_RB  :BOOL;        //EPICS Status variable: L2_Monitor_En_RB
      H2_RB  :REAL;        //EPICS Status variable: H2_RB
      H1_RB  :REAL;        //EPICS Status variable: H1_RB
      L1_RB  :REAL;        //EPICS Status variable: L1_RB
      L2_RB  :REAL;        //EPICS Status variable: L2_RB
      H2_Act  :BOOL;        //EPICS Status variable: H2_Act
      H1_Act  :BOOL;        //EPICS Status variable: H1_Act
      L1_Act  :BOOL;        //EPICS Status variable: L1_Act
      L2_Act  :BOOL;        //EPICS Status variable: L2_Act
      H2_Lchd  :BOOL;        //EPICS Status variable: H2_Lchd
      H1_Lchd  :BOOL;        //EPICS Status variable: H1_Lchd
      L1_Lchd  :BOOL;        //EPICS Status variable: L1_Lchd
      L2_Lchd  :BOOL;        //EPICS Status variable: L2_Lchd
      FaultLatching_En_RB  :BOOL;        //EPICS Status variable: FaultLatching_En_RB
      GroupFault_Act  :BOOL;        //EPICS Status variable: GroupFault_Act
      RangeMin_RB  :REAL;        //EPICS Status variable: RangeMin_RB
      RangeMax_RB  :REAL;        //EPICS Status variable: RangeMax_RB
END_VAR
VAR_OUTPUT
      OpMode_CmdFreerun  :BOOL;        //EPICS Command variable: OpMode_CmdFreerun
      OpMode_CmdForce  :BOOL;        //EPICS Command variable: OpMode_CmdForce
      ForceValue_CmdAct  :BOOL;        //EPICS Command variable: ForceValue_CmdAct
      FaultLatchLim_CmdRS  :BOOL;        //EPICS Command variable: FaultLatchLim_CmdRS
      H2_SP  :REAL;        //EPICS Parameter variable: H2_SP
      H1_SP  :REAL;        //EPICS Parameter variable: H1_SP
      L1_SP  :REAL;        //EPICS Parameter variable: L1_SP
      L2_SP  :REAL;        //EPICS Parameter variable: L2_SP
      ForceValue_SP  :REAL;        //EPICS Parameter variable: ForceValue_SP
END_VAR
VAR
	nTempUINT		:UINT;
	i			:INT;
	sTempHStr		:T_MaxString;

	uREAL2UINTs	:U_REAL_UINTs;
	uUINTs2REAL	:U_REAL_UINTs;
	uTIME2UINTs	:U_TIME_UINTs;
	uUINTs2TIME	:U_TIME_UINTs;
	uDINT2UINTs	:U_DINT_UINTs;
	uUINTs2DINT	:U_DINT_UINTs;
	fValue: INT;
END_VAR

]]></Declaration>
    <Implementation>
      <ST><![CDATA[
(*
**********************EPICS<-->Beckhoff integration at ESS in Lund, Sweden*******************************
Data types handler for TCP/IP communication EPICS<--Beckhoff at ESS. Lund, Sweden.
Created by: Andres Quintanilla (andres.quintanilla@esss.se)
            Miklos Boros (miklos.boros@esss.se)
Notes: Converts different types of data into UINT. Adds the converted data into the array to be sent to EPICS.
The first 10 spaces of the array are reserved. nOffset input is used for that. 
Code must not be changed manually. Code is generated and handled by PLC factory at ESS.
Versions:
Version 1: 06/04/2018. Communication stablished and stable
**********************************************************************************************************
*)

    //********************************************
    //*************STATUS VARIABLES***************
    //********************************************


       STRNCPY(ADR(EPICS_GVL.aDataS7) + SIZEOF(UINT) * (nOffsetStatus + 0), ADR(HWIO_TerminalName), 40);

       STRNCPY(ADR(EPICS_GVL.aDataS7) + SIZEOF(UINT) * (nOffsetStatus + 20), ADR(HWIO_ChannelNo), 40);

       nTempUINT.0           := HWIO_Fault_Act;       //EPICSName: HWIO_Fault_Act
       nTempUINT.1           := HWIO_Fault_Lchd;       //EPICSName: HWIO_Fault_Lchd
       nTempUINT.2           := Force_En_RB;       //EPICSName: Force_En_RB
       nTempUINT.3           := OpMode_FreeRun_Act;       //EPICSName: OpMode_FreeRun_Act
       nTempUINT.4           := OpMode_Forced_Act;       //EPICSName: OpMode_Forced_Act
       EPICS_GVL.aDataS7[nOffsetStatus + 40]    := nTempUINT;

       uREAL2UINTs.fValue :=ForceValue_RB;
       EPICS_GVL.aDataS7[nOffsetStatus + 41]           := uREAL2UINTs.stLowHigh.nLow;       //EPICSName: ForceValue_RB
       EPICS_GVL.aDataS7[nOffsetStatus + 42]           := uREAL2UINTs.stLowHigh.nHigh;       //EPICSName: ForceValue_RB

       uREAL2UINTs.fValue :=MeasValue;
       EPICS_GVL.aDataS7[nOffsetStatus + 43]           := uREAL2UINTs.stLowHigh.nLow;       //EPICSName: MeasValue
       EPICS_GVL.aDataS7[nOffsetStatus + 44]           := uREAL2UINTs.stLowHigh.nHigh;       //EPICSName: MeasValue

       nTempUINT.0           := H2_Monitor_En_RB;       //EPICSName: H2_Monitor_En_RB
       nTempUINT.1           := H1_Monitor_En_RB;       //EPICSName: H1_Monitor_En_RB
       nTempUINT.2           := L1_Monitor_En_RB;       //EPICSName: L1_Monitor_En_RB
       nTempUINT.3           := L2_Monitor_En_RB;       //EPICSName: L2_Monitor_En_RB
       EPICS_GVL.aDataS7[nOffsetStatus + 45]    := nTempUINT;

       uREAL2UINTs.fValue :=H2_RB;
       EPICS_GVL.aDataS7[nOffsetStatus + 46]           := uREAL2UINTs.stLowHigh.nLow;       //EPICSName: H2_RB
       EPICS_GVL.aDataS7[nOffsetStatus + 47]           := uREAL2UINTs.stLowHigh.nHigh;       //EPICSName: H2_RB

       uREAL2UINTs.fValue :=H1_RB;
       EPICS_GVL.aDataS7[nOffsetStatus + 48]           := uREAL2UINTs.stLowHigh.nLow;       //EPICSName: H1_RB
       EPICS_GVL.aDataS7[nOffsetStatus + 49]           := uREAL2UINTs.stLowHigh.nHigh;       //EPICSName: H1_RB

       uREAL2UINTs.fValue :=L1_RB;
       EPICS_GVL.aDataS7[nOffsetStatus + 50]           := uREAL2UINTs.stLowHigh.nLow;       //EPICSName: L1_RB
       EPICS_GVL.aDataS7[nOffsetStatus + 51]           := uREAL2UINTs.stLowHigh.nHigh;       //EPICSName: L1_RB

       uREAL2UINTs.fValue :=L2_RB;
       EPICS_GVL.aDataS7[nOffsetStatus + 52]           := uREAL2UINTs.stLowHigh.nLow;       //EPICSName: L2_RB
       EPICS_GVL.aDataS7[nOffsetStatus + 53]           := uREAL2UINTs.stLowHigh.nHigh;       //EPICSName: L2_RB

       nTempUINT.0           := H2_Act;       //EPICSName: H2_Act
       nTempUINT.1           := H1_Act;       //EPICSName: H1_Act
       nTempUINT.2           := L1_Act;       //EPICSName: L1_Act
       nTempUINT.3           := L2_Act;       //EPICSName: L2_Act
       nTempUINT.4           := H2_Lchd;       //EPICSName: H2_Lchd
       nTempUINT.5           := H1_Lchd;       //EPICSName: H1_Lchd
       nTempUINT.6           := L1_Lchd;       //EPICSName: L1_Lchd
       nTempUINT.7           := L2_Lchd;       //EPICSName: L2_Lchd
       nTempUINT.8           := FaultLatching_En_RB;       //EPICSName: FaultLatching_En_RB
       nTempUINT.9           := GroupFault_Act;       //EPICSName: GroupFault_Act
       EPICS_GVL.aDataS7[nOffsetStatus + 54]    := nTempUINT;

       uREAL2UINTs.fValue :=RangeMin_RB;
       EPICS_GVL.aDataS7[nOffsetStatus + 55]           := uREAL2UINTs.stLowHigh.nLow;       //EPICSName: RangeMin_RB
       EPICS_GVL.aDataS7[nOffsetStatus + 56]           := uREAL2UINTs.stLowHigh.nHigh;       //EPICSName: RangeMin_RB

       uREAL2UINTs.fValue :=RangeMax_RB;
       EPICS_GVL.aDataS7[nOffsetStatus + 57]           := uREAL2UINTs.stLowHigh.nLow;       //EPICSName: RangeMax_RB
       EPICS_GVL.aDataS7[nOffsetStatus + 58]           := uREAL2UINTs.stLowHigh.nHigh;       //EPICSName: RangeMax_RB

    //********************************************
    //*************COMMAND VARIABLES**************
    //********************************************


       nTempUINT			:= EPICS_GVL.aDataModbus[nOffsetCmd + 0];
       OpMode_CmdFreerun             :=     nTempUINT.0;       //EPICSName: OpMode_CmdFreerun
       OpMode_CmdForce             :=     nTempUINT.1;       //EPICSName: OpMode_CmdForce
       ForceValue_CmdAct             :=     nTempUINT.2;       //EPICSName: ForceValue_CmdAct
       FaultLatchLim_CmdRS             :=     nTempUINT.3;       //EPICSName: FaultLatchLim_CmdRS
       if (EPICS_GVL.EasyTester <> 2) THEN EPICS_GVL.aDataModbus[nOffsetCmd + 0]:=0; END_IF

    //********************************************
    //************PARAMETER VARIABLES*************
    //********************************************


       uUINTs2REAL.stLowHigh.nLow             := EPICS_GVL.aDataModbus[nOffsetCmd + 1];       //EPICSName: H2_SP
       uUINTs2REAL.stLowHigh.nHigh             := EPICS_GVL.aDataModbus[nOffsetCmd + 2];       //EPICSName: H2_SP
       H2_SP				:= uUINTs2REAL.fValue;

       uUINTs2REAL.stLowHigh.nLow             := EPICS_GVL.aDataModbus[nOffsetCmd + 3];       //EPICSName: H1_SP
       uUINTs2REAL.stLowHigh.nHigh             := EPICS_GVL.aDataModbus[nOffsetCmd + 4];       //EPICSName: H1_SP
       H1_SP				:= uUINTs2REAL.fValue;

       uUINTs2REAL.stLowHigh.nLow             := EPICS_GVL.aDataModbus[nOffsetCmd + 5];       //EPICSName: L1_SP
       uUINTs2REAL.stLowHigh.nHigh             := EPICS_GVL.aDataModbus[nOffsetCmd + 6];       //EPICSName: L1_SP
       L1_SP				:= uUINTs2REAL.fValue;

       uUINTs2REAL.stLowHigh.nLow             := EPICS_GVL.aDataModbus[nOffsetCmd + 7];       //EPICSName: L2_SP
       uUINTs2REAL.stLowHigh.nHigh             := EPICS_GVL.aDataModbus[nOffsetCmd + 8];       //EPICSName: L2_SP
       L2_SP				:= uUINTs2REAL.fValue;

       uUINTs2REAL.stLowHigh.nLow             := EPICS_GVL.aDataModbus[nOffsetCmd + 9];       //EPICSName: ForceValue_SP
       uUINTs2REAL.stLowHigh.nHigh             := EPICS_GVL.aDataModbus[nOffsetCmd + 10];       //EPICSName: ForceValue_SP
       ForceValue_SP				:= uUINTs2REAL.fValue;

]]></ST>
    </Implementation>
    <LineIds Name="FB_DEVTYPE_TS_AnalogInput_PT100">
      <LineId Id="3" Count="116" />
      <LineId Id="2" Count="0" />
    </LineIds>
  </POU>
</TcPlcObject>